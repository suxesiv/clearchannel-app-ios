//
//  ProductPriceComplex.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 23.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductPriceInterface.h"

@interface ProductPriceComplex : ProductPriceInterface

@property (nonatomic) int productId;
@property (nonatomic) double base;
@property (nonatomic) double piece;
@property (nonatomic) double plateLanguage;
@property (nonatomic) double plateSujet;

- (int) getPlatesByIndex: (int) index forSujets: (NSNumber *) numSujet;

@end
