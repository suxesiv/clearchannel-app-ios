//
//  Calculation_Item.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 22.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CalculationItemConfig;
@class Product;

@interface CalculationItem : NSObject

@property (nonatomic) int productId;
@property (nonatomic, strong) NSMutableDictionary *configs;

- (id) initWithProduct: (Product *) product;

#pragma mark configuration
- (CalculationItemConfig *) getConfig: (NSString *) language;
- (unsigned int) getTotalQuantity;
- (NSArray *) getSujetCounts;
- (unsigned int) getTotalSujets;
- (unsigned int) getMaxSujets;
- (unsigned int) getMinQuantity;
- (unsigned int) getTotalLanguages;

#pragma mark product
+ (CalculationItem *) createWithProduct: (Product *) product;
- (Product *) getProduct;

#pragma mark price
- (double) getPrice;

@end
