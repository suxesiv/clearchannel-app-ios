//
//  ProductConfigureColumn.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 21.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductConfigureColumn.h"

#import "ProductViewController.h"

@implementation ProductConfigureColumn

@synthesize delegate;
@synthesize contentView;
@synthesize titleLabel;
@synthesize sujetStepper;
@synthesize sujetLabel;
@synthesize buttonQuantity;

// values
@synthesize language;
@synthesize quantity;
@synthesize sujets;

- (void)awakeFromNib {
    [[NSBundle mainBundle] loadNibNamed: @"ProductConfigureColumn" owner: self options: nil];
    
    // replace fonts
    // FIXME remove as soon Apple allows it to set custom fonts in IB
    SET_BOLD_FONT(titleLabel);
    SET_BOLD_FONT(buttonQuantity.titleLabel);
    SET_BOLD_FONT(sujetLabel);
    
    titleLabel.textColor =
    sujetLabel.textColor =
    sujetStepper.tintColor = TEXT_COLOR;
    buttonQuantity.backgroundColor = BASE_COLOR;
    
    [self reset];
    
    [self addSubview: self.contentView];
}

- (void) reset {
    [self setSujets: 0 andQuantity: 0];
}

- (void) setSujets: (unsigned int) newSujets andQuantity: (unsigned int) newQuantity {
    sujets = newSujets;
    quantity = newQuantity;
    
    sujetStepper.value = sujets;
    sujetLabel.text = [NSString stringWithFormat: @"%d", sujets];
    [buttonQuantity setTitle: [NSString stringWithFormat: @"%d", quantity] forState: UIControlStateNormal];
}

- (void) updateQuantity: (unsigned int) newQuantity {
    quantity = newQuantity;
    [buttonQuantity setTitle: [NSString stringWithFormat: @"%d", quantity] forState: UIControlStateNormal];
    [self didChange];
}

- (IBAction)eventQuantityChange:(UITextField *)sender {
    //quantity = (unsigned int) quantityInput.text.longLongValue;
    [self didChange];
}

- (IBAction)eventSujetChange:(UIStepper *)sender {
    sujets = (unsigned int) sender.value;
    sujetLabel.text = [NSString stringWithFormat: @"%.0f", sender.value];
    [self didChange];
}

- (IBAction)eventClickQuantityButton:(UIButton *)sender {
    if (delegate != nil) {
        [delegate showNumberPickerFor: sender inConfig: self withValue: quantity];
    }
}

- (void) didChange {
    if (delegate != nil) {
        [delegate configDidChange: self];
    } else {
        NSLog(@"%s – have no delegate", __PRETTY_FUNCTION__);
    }
}

@end
