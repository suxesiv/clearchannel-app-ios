//
//  ProductButton.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 19.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Product.h"

@interface ProductListItem : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end
