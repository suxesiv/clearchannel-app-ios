//
//  InfoViewController.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 23.09.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

@end
