//
//  BaseViewController.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 19.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "BaseViewController.h"

#import "Calculation.h"

@implementation BaseViewController

@synthesize calculationBarButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (calculationBarButton != nil) {
        // register event
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(eventChangeCalculation:) name: EVENT_CHANGE_CALCULATION object: nil];
        
        [self updateCalculationButton];
    }
}

- (void) eventChangeCalculation: (NSNotification *) notification {
    [self updateCalculationButton];
}

- (void) updateCalculationButton {
    unsigned int numItems = [[Calculation sharedInstance] numItems];
    calculationBarButton.title = [NSString stringWithFormat: @"%d Formate", numItems];
    calculationBarButton.enabled = numItems > 0;
}

- (IBAction)eventShowCalculation:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName: EVENT_SHOW_CALCULATION object: nil];
}

@end
