//
//  Calculation_Item.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 22.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "CalculationItem.h"

#import "ProductManager.h"
#import "Product.h"
#import "CalculationItemConfig.h"

@implementation CalculationItem

@synthesize productId;
@synthesize configs;

- (id)init {
    if (self = [super init]) {
        configs = [NSMutableDictionary dictionaryWithCapacity: 3];
    }
    return self;
}

- (id) initWithProduct: (Product *) product {
    if (self = [self init]) {
        self.productId = product.productId;
    }
    return self;
}

#pragma mark configuration
- (CalculationItemConfig *) getConfig: (NSString *) language {
    CalculationItemConfig *config = [configs objectForKey: language];
    if (config == nil) {
        config = [[CalculationItemConfig alloc] init];
        config.language = language;
        [configs setObject: config forKey: language];
    }
    return config;
}

- (unsigned int) getTotalQuantity {
    unsigned int quantity = 0;
    
    for (NSString *key in configs) {
        CalculationItemConfig *config = [configs objectForKey: key];
        quantity += config.quantity;
    }
    
    return quantity;
}

- (NSArray *) getSujetCounts {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity: configs.count];
    
    for (NSString *key in configs) {
        CalculationItemConfig *config = [configs objectForKey: key];
        if (config.sujets) {
            [result addObject: [NSNumber numberWithUnsignedInt: config.sujets]];
        }
    }
    
    return [[[result sortedArrayUsingSelector:@selector(compare:)] reverseObjectEnumerator] allObjects];
}

- (unsigned int) getTotalSujets {
    unsigned int sujets = 0;
    
    for (NSNumber *count in [self getSujetCounts]) {
        sujets += count.unsignedIntValue;
    }
    
    return sujets;
}

- (unsigned int) getMaxSujets
{
    return ((NSNumber *) [[self getSujetCounts] valueForKeyPath: @"@max.self"]).unsignedIntValue;
}

/**
 * THIS METHOD differs to the php implementation in the following way
 * return value is unsigned and therfore is not -1 if no configurations are found
 * if no configurations are found the return value is 0 not -1
 */
- (unsigned int) getMinQuantity {
    unsigned int quantity = 0;
    
    for (NSString *key in configs) {
        CalculationItemConfig *config = [configs objectForKey: key];
        if (config.quantity > 0 && quantity > config.quantity) {
            quantity = config.quantity;
        }
    }
    
    return quantity;
}

- (unsigned int) getTotalLanguages
{
    unsigned int languages = 0;
    
    for (NSString *key in configs) {
        CalculationItemConfig *config = [configs objectForKey: key];
        if (config.sujets) {
            ++languages;
        }
    }
    
    return languages;
}

#pragma mark product
+ (CalculationItem *) createWithProduct: (Product *) product {
    return [[self alloc] initWithProduct: product];
}

- (Product *) getProduct {
    if (productId > 0) {
        return [[ProductManager sharedInstance] getProductById: productId];
    }
    
    return nil;
}

#pragma mark price
- (double) getPrice {
    return [[self getProduct] getPrice: self];
}

@end
