//
//  ProductButton.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 19.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductListItem.h"

@implementation ProductListItem

@synthesize imageView;
@synthesize titleLabel;
@synthesize infoLabel;

@end
