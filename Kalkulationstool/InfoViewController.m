//
//  InfoViewController.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 23.09.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "InfoViewController.h"


@implementation InfoViewController

@synthesize contentTextView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    contentTextView.text = CALCULATION_DISCLAIMER;
}

@end
