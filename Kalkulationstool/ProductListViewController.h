//
//  ProductListViewController.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 19.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "BaseViewController.h"

#import "Product.h"
#import "ProductListContainer.h"

@interface ProductListViewController : UICollectionViewController {
    Product *selectProduct;
    
    NSInteger detailListSpacerIndex;
}

@property (strong, nonatomic) NSMutableArray *products;
@property (nonatomic) BOOL isDetailList;

- (void) selectProduct: (Product *) product;

@end
