//
//  BigNumberPickerViewController.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 05.06.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ProductConfigureColumn.h"

@interface BigNumberPickerViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) ProductConfigureColumn *delegate; // FIXME hardcoded view as delegate
@property (nonatomic, strong) UIPickerView *pickerView;

- (void) setPickerValue: (uint) value;

@end
