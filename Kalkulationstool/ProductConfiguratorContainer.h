//
//  ProductConfiguratorContainer.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 21.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductConfiguratorContainer : UIView {
    UIView *headContainer;
    
    UIView *bodyContainer;
    
    UIView *footContainer;
}

@end
