//
//  CurrencyFormatter.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 23.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "CurrencyFormatter.h"

@implementation CurrencyFormatter

- (id)init {
    if (self = [super init]) {
        [self setLocale: [NSLocale localeWithLocaleIdentifier: @"de_CH"]]; // FIXME hardcoded CHF
        [self setNumberStyle: NSNumberFormatterCurrencyStyle];
        //[self setCurrencySymbol: @""];
    }
    return self;
}

- (BOOL) isFraction: (NSNumber *) number {
    double value = number.doubleValue;
    return floor(value) != value;
}

- (NSString *)stringFromNumber:(NSNumber *)number {
    
    // round to 5er
    number = [NSNumber numberWithDouble: round(number.doubleValue * 20) / 20];
    
    //if ([self isFraction: number]) {
        // has something after the comma
        [self setMinimumFractionDigits: 2];
        return [super stringFromNumber: number];
    /*} else {
        // has XXX.00 but make the designer happy with it XXX.–
        [self setMaximumFractionDigits: 0];
        return [NSString stringWithFormat: @"%@.–", [super stringFromNumber: number]];
    }*/
}

@end
