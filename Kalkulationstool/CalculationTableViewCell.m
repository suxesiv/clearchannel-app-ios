//
//  CalculationTableViewCell.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 26.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "CalculationTableViewCell.h"

@implementation CalculationTableViewCell

@synthesize labelProduct, labelProductInfo, labelPrice;

@synthesize labelLanguage0, labelLanguage1, labelLanguage2;
@synthesize labelSujet0, labelSujet1, labelSujet2;
@synthesize labelQuantity0, labelQuantity1, labelQuantity2;

- (void)awakeFromNib
{
    // Initialization code
    // replace fonts
    // FIXME remove as soon Apple allows it to set custom fonts in IB
    SET_BOLDER_FONT(labelProduct);
    SET_REGULAR_FONT(labelProductInfo);
    SET_BOLDER_FONT(labelPrice);
    
    SET_BOLD_FONT(labelLanguage0);
    SET_BOLD_FONT(labelLanguage1);
    SET_BOLD_FONT(labelLanguage2);
    
    SET_BOLD_FONT(labelSujet0);
    SET_BOLD_FONT(labelSujet1);
    SET_BOLD_FONT(labelSujet2);
    
    SET_BOLD_FONT(labelQuantity0);
    SET_BOLD_FONT(labelQuantity1);
    SET_BOLD_FONT(labelQuantity2);
    
    labelProduct.textColor =
    labelProductInfo.textColor =
    labelPrice.textColor =
    
    labelLanguage0.textColor =
    labelLanguage1.textColor =
    labelLanguage2.textColor =
    
    labelSujet0.textColor =
    labelSujet1.textColor =
    labelSujet2.textColor =
    
    labelQuantity0.textColor =
    labelQuantity1.textColor =
    labelQuantity2.textColor = TEXT_COLOR;
}
@end
