//
//  Product.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 19.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "Product.h"

#import "CalculationItem.h"
#import "CalculationItemConfig.h"

#import "ProductPriceInterface.h"
#import "ProductPriceSimple.h"
#import "ProductPriceTier.h"
#import "ProductPriceComplex.h"

@implementation Product

@synthesize productId;
@synthesize name;
@synthesize description;
@synthesize info;
@synthesize iconName;

@synthesize priceType;
@synthesize tierPriceLowQuantity;

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (double) getPrice: (CalculationItem *) item {
    // check if configured
    if ([item getTotalQuantity] <= 0 || [item getTotalSujets] <= 0) {
        //NSLog(@"%s: empty config (%d / %d)", __PRETTY_FUNCTION__, [item getTotalQuantity], [item getTotalSujets]);
        return 0;
    }
    
    double price = 0;
    
    /**
     * split price calculation
     * some languages can be calculated with tier price others combined with complex or simple price
     */
    if (
        tierPriceLowQuantity && // if product has low quantity
        SHOP_LOW_QUANTITY > 0 && // if low quantity is defined
        [item getMinQuantity] < SHOP_LOW_QUANTITY // and min quantity is low
    ) {
        // pretty complex way of doing it
        NSMutableArray *types = [NSMutableArray arrayWithCapacity: 1];
        NSMutableDictionary *configsByType = [NSMutableDictionary dictionaryWithCapacity: 1];
        
        for (NSString *key in item.configs) {
            CalculationItemConfig *config = [item.configs objectForKey: key];
            NSString *type = [self getPriceInstanceType: config.quantity];
            if (![types containsObject: type]) { // TODO test if containsObject works as expected
                [types addObject: type];
            }
            
            NSMutableArray *configArray = [configsByType objectForKey: type];
            if (!configArray) {
                configArray = [NSMutableArray arrayWithCapacity: 3];
                [configsByType setObject: configArray forKey: type];
            }
            
            [configArray addObject: config];
        }
        
        // calculation
        for (NSString *type in types) {
            NSMutableArray *typeConfigs = [configsByType objectForKey: type];
            ProductPriceInterface *priceInstance = [self getPriceInstanceByType: type];
            
            // create dummy item for price calculation
            CalculationItem *dummyItem = [[CalculationItem alloc] init];
            for (CalculationItemConfig *config in typeConfigs) {
                CalculationItemConfig *dummyConfig = [dummyItem getConfig: config.language];
                
                // copy data
                dummyConfig.sujets = config.sujets;
                dummyConfig.quantity = config.quantity;
            }
            
            // get prices
            price += [priceInstance calculatePrice: dummyItem];
        }
    } else {
        // simple way
        ProductPriceInterface *priceInstance = [self getPriceInstance: [item getTotalQuantity]];
        price = [priceInstance calculatePrice: item];
    }
    
    return price;
}

- (ProductPriceInterface *) getPriceInstance: (unsigned int) quantity {
    return [self getPriceInstanceByType: [self getPriceInstanceType: quantity]];
}

/**
 * The implementation of this method differs to the php counterpart in the following way:
 * - The php implementation creates an empty price type if none was found in the database
 *   the objc method does not and returns nil instead!
 */
- (ProductPriceInterface *) getPriceInstanceByType: (NSString *) type {
    
    if ([type isEqualToString: SHOP_PRICE_TYPE_SIMPLE]) {
        return [ProductPriceSimple priceTypeForProduct: self.productId];
    } else if ([type isEqualToString: SHOP_PRICE_TYPE_COMPLEX]) {
        return [ProductPriceComplex priceTypeForProduct: self.productId];
    } else if ([type isEqualToString: SHOP_PRICE_TYPE_TIER]) {
        return [ProductPriceTier priceTypeForProduct: self.productId];
    } else {
        @throw @"undefined price type";
    }
    
    return nil;
}

- (NSString *) getPriceInstanceType: (unsigned int) quantity {
    NSString *type = self.priceType;
    
    if (
        quantity > 0 && // if quantity is set
        tierPriceLowQuantity && // and tier_price_low_quantity is active
        SHOP_LOW_QUANTITY > 0 && // and shop config defines a low quantity
        quantity < SHOP_LOW_QUANTITY // and the given quantity is less than the low quantity
    ) {
        type = SHOP_PRICE_TYPE_TIER;
    }
    
    return type;
}

@end
