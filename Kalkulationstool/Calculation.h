//
//  Calculation.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 22.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CalculationItem.h"

@interface Calculation : NSObject {
    NSMutableArray *items;
}

+ (Calculation *)sharedInstance;

- (void) addItem: (CalculationItem *) item;
- (NSMutableArray *) getItems;
- (unsigned int) numItems;
- (double) getTotalPrice;

#pragma mark methods without counterparts in php
- (CalculationItem *) getItemAtIndex: (NSUInteger) index;
- (void) removeItemAtIndex: (NSUInteger) index;

- (void) empty;

@end
