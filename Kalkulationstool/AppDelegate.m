//
//  AppDelegate.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 19.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    
    
    // load storyboard
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName: @"Main" bundle: nil];
    rootViewController = [storyboard instantiateInitialViewController];
    
    // main view controller
    mainViewController = [storyboard instantiateViewControllerWithIdentifier: @"MainViewController"];
    
    // product view controller
    productViewController = [storyboard instantiateViewControllerWithIdentifier: @"ProductViewController"];
    
    // calculation view controller
    calculationViewController = [storyboard instantiateViewControllerWithIdentifier: @"CalculationViewController"];
    
    // setup
    [rootViewController pushViewController: mainViewController animated: NO];

    // init notification center
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver: self selector: @selector(eventShowMain:) name: EVENT_SHOW_MAIN object: nil];
    [center addObserver: self selector: @selector(eventShowProductDetail:) name: EVENT_PRODUCT_DETAIL object: nil];
    [center addObserver: self selector: @selector(eventShowCalculation:) name: EVENT_SHOW_CALCULATION object: nil];
    
    self.window.tintColor = BASE_COLOR;
    rootViewController.navigationBar.tintColor = BASE_COLOR;
    
    [self.window setRootViewController: rootViewController];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) eventShowMain: (NSNotification *) notification {
    [rootViewController popToRootViewControllerAnimated: YES];
}

- (void) eventShowProductDetail: (NSNotification *) notification {
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    
    CalculationItem *item = [notification.userInfo objectForKey: @"item"];
    if (item != nil) {
        [productViewController setCalculationItemAndSelect: item];
    } else {
        Product *product = [notification.userInfo objectForKey: @"product"];
        [productViewController setAndSelectProduct: product];
    }
    
    if ([rootViewController.viewControllers containsObject: productViewController]) {
        [rootViewController popToViewController: productViewController animated: YES];
    } else {
        [rootViewController pushViewController: productViewController animated: YES];
    }
}

- (void) eventShowCalculation: (NSNotification *) notification {
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if ([rootViewController.viewControllers containsObject: calculationViewController]) {
        [rootViewController popToViewController: calculationViewController animated: YES];
    } else {
        [rootViewController pushViewController: calculationViewController animated: YES];
    }
}

@end
