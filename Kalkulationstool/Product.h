//
//  Product.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 19.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CalculationItem;
@class ProductPriceInterface;

@interface Product : NSObject

// general info
@property (nonatomic) int productId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *info;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *iconName;

@property (nonatomic, strong) NSString *priceType;
@property (nonatomic) BOOL tierPriceLowQuantity;

// price calculation
- (double) getPrice: (CalculationItem *) item;
- (ProductPriceInterface *) getPriceInstance: (unsigned int) quantity;
- (NSString *) getPriceInstanceType: (unsigned int) quantity;
- (ProductPriceInterface *) getPriceInstanceByType: (NSString *) type;

@end
