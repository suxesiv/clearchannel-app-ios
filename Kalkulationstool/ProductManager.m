//
//  ProductManager.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 19.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductManager.h"

#import "Product.h"

static ProductManager *sharedProductManager = nil;

@implementation ProductManager

@synthesize database;

- (id)init {
    if (self = [super init]) {
        // load database
        // http://stackoverflow.com/questions/8900647/ios-fmdb-load-db-from-file
        // with this path the database can only read not written! (see link)
        NSString *path = [[NSBundle mainBundle] pathForResource:@"product-database" ofType:@"sqlite3"];
        database = [FMDatabase databaseWithPath: path];
        
        if (![database open]) {
            NSLog(@"could not open database");
            [NSException raise:NSInvalidArgumentException format:@"unable to open database"];
            database = nil;
            return nil;
        }
        
        [database setCrashOnErrors: YES];
    }
    return self;
}

+(ProductManager *)sharedInstance {
    @synchronized(self) {
        if (sharedProductManager == nil) {
            sharedProductManager = [[self alloc] init];
        }
    }
    return sharedProductManager;
}

- (NSMutableArray *) getProducts {
    @synchronized(products) {
        if (products == nil) {
            products = [NSMutableArray arrayWithCapacity: 10];
            
            // query products form database
            FMResultSet *rs = [database executeQuery:@"SELECT rowid, * FROM product ORDER BY rank"];
            while ([rs next]) {
                Product *product = [[Product alloc] init];
                
                product.productId = [rs intForColumn: @"rowid"];
                product.name = [rs stringForColumn: @"name"];
                product.description = [rs stringForColumn: @"description"];
                product.info = [rs stringForColumn: @"info"];
                product.iconName = [NSString stringWithFormat: @"product-icon-%@", [rs stringForColumn: @"icon"]];
                
                product.priceType = [rs stringForColumn: @"price_type"];
                product.tierPriceLowQuantity = [rs boolForColumn: @"tier_price_low_quantity"];
                
                [products addObject: product];
            }
            // close the result set.
            // it'll also close when it's dealloc'd, but we're closing the database before
            // the autorelease pool closes, so sqlite will complain about it.
            [rs close];
            
        }
    }
    
    return products;
}

- (Product *) getProductById: (int) productId {
    for (Product *product in [self getProducts]) {
        if (product.productId == productId) {
            return product;
        }
    }
    return nil;
}

@end
