//
//  ProductPriceTierContainer.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 23.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductPriceInterface.h"

/**
 * This is a combination of the Shop_Model_Product_Price_Tier_Container and _Config classes
 */
@interface ProductPriceTier : ProductPriceInterface

@property (nonatomic) int productId;
@property (nonatomic) double base;
@property (nonatomic) double priceSujet;

- (double) getPriceForQuantity: (unsigned int) quantity;

@end
