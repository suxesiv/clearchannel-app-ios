//
//  Calculation.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 22.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "Calculation.h"

static Calculation *sharedCalculation = nil;

@implementation Calculation

- (id)init {
    if (self = [super init]) {
        items = [NSMutableArray arrayWithCapacity: 1];
    }
    return self;
}

+(Calculation *)sharedInstance {
    @synchronized(self) {
        if (sharedCalculation == nil) {
            sharedCalculation = [[self alloc] init];
        }
    }
    return sharedCalculation;
}

/**
 * Other than the php implementation this method checks if the item is already in the calculation
 * if so, this call gets ignored
 */
- (void) addItem: (CalculationItem *) item {
    //NSLog(@"addItem with ProductId: %d", item.productId);
    if (![items containsObject: item]) {
        [items addObject: item];   
    }
}

- (NSMutableArray *) getItems {
    return items;
}

- (unsigned int) numItems {
    return (unsigned int)items.count;
}

- (double) getTotalPrice {
    double price = 0;
    
    for (CalculationItem *item in items) {
        price += [item getPrice];
    }
    
    return price;
}

- (void) empty {
    items = [NSMutableArray arrayWithCapacity: 1];
}

#pragma mark methods without counterparts in php
- (CalculationItem *) getItemAtIndex: (NSUInteger) index {
    // TODO check array bounds!
    return [items objectAtIndex: index];
}

- (void) removeItemAtIndex: (NSUInteger) index {
    [items removeObjectAtIndex: index];
}

@end
