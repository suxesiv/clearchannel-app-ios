//
//  ProductConfigureHeader.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 21.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductConfigureHeader.h"

@implementation ProductConfigureHeader

@synthesize contentView;
@synthesize labelLanguage, labelQuantity, labelSujet;

- (void)awakeFromNib {
    [[NSBundle mainBundle] loadNibNamed: @"ProductConfigureHeader" owner: self options: nil];
    
    // replace fonts
    // FIXME remove as soon Apple allows it to set custom fonts in IB
    SET_REGULAR_FONT(labelLanguage);
    SET_REGULAR_FONT(labelQuantity);
    SET_REGULAR_FONT(labelSujet);
    
    labelLanguage.textColor =
    labelQuantity.textColor =
    labelSujet.textColor = TEXT_COLOR;
    
    [self addSubview: self.contentView];
}

@end
