//
//  ProductPriceRule.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 27.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CalculationItem.h"

@interface ProductPriceRule : NSObject {
}

@property (nonatomic) int ruleId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *type;
@property (nonatomic) unsigned int languages;
@property (nonatomic) unsigned int sujets;
@property (nonatomic, strong) NSString *priceType;
@property (nonatomic) double percent;
@property (nonatomic) double amount;

- (BOOL) appliesToItem: (CalculationItem *) item;
- (double) calculateAmount: (double) price;
+ (NSMutableArray *) getByProductId: (int) productId andPriceType: (NSString *) priceType;

@end
