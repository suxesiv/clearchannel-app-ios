//
//  ProductViewController.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 20.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Product.h"
#import "ProductListViewController.h"
#import "ProductConfigureColumn.h"
#import "ProductConfigureDelegate.h"

#import "Calculation.h"
#import "CurrencyFormatter.h"

#import "BigNumberPickerViewController.h"

@interface ProductViewController : BaseViewController <ProductConfigureDelegate> {
    Product *product;
    ProductListViewController *listViewController;
    
    CalculationItem *calculationItem;
    
    CurrencyFormatter *priceFormatter;
    BOOL selectProduct;
    
    // number picker
    UIPopoverController *numberPickerPopover;
    BigNumberPickerViewController *numberPickerViewController;
    
}

// views
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

// configurator
@property (weak, nonatomic) IBOutlet ProductConfigureColumn *configView0;
@property (weak, nonatomic) IBOutlet ProductConfigureColumn *configView1;
@property (weak, nonatomic) IBOutlet ProductConfigureColumn *configView2;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

- (void) setCalculationItemAndSelect: (CalculationItem *) item;
- (void) setProduct: (Product *) newProduct;
- (void) setAndSelectProduct: (Product *) newProduct;
- (void) updateProductViews;
- (IBAction)eventAddToCalculation:(id)sender;

@end
