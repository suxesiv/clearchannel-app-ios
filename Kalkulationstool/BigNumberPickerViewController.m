//
//  BigNumberPickerViewController.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 05.06.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "BigNumberPickerViewController.h"

@implementation BigNumberPickerViewController

@synthesize delegate;
@synthesize pickerView;

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // init picker
    pickerView = [[UIPickerView alloc] init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    [self.view addSubview: pickerView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    pickerView.frame = CGRectMake(0, 0, 300, 216); // FIXME hardcoded size
}

- (void) setPickerValue: (uint) value {
    [pickerView selectRow: (int)round((value / 1000) % 10) inComponent: 0 animated: NO];
    [pickerView selectRow: (int)round((value / 100) % 10) inComponent: 1 animated: NO];
    [pickerView selectRow: (int)round((value / 10) % 10) inComponent: 2 animated: NO];
    [pickerView selectRow: (int)round((value / 1) % 10) inComponent: 3 animated: NO];
}

#pragma mark UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 4;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 10;
}

#pragma mark UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat: @"%ld", (long)row];
}

- (void)pickerView:(UIPickerView *)selectedPickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    int value = 0;
    
    value += [selectedPickerView selectedRowInComponent: 0] * 1000;
    value += [selectedPickerView selectedRowInComponent: 1] * 100;
    value += [selectedPickerView selectedRowInComponent: 2] * 10;
    value += [selectedPickerView selectedRowInComponent: 3] * 1;
    
    //NSLog(@"value: %d", value);
    if (delegate != nil) {
        [delegate updateQuantity: (unsigned int) value];
    }
}

@end
