//
//  CalculationViewController.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 20.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#import "Calculation.h"

@interface CalculationViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate> {
    Calculation *calculation;
    double deliveryCost;
}

@property (weak, nonatomic) IBOutlet UILabel *labelHeaderFormat;
@property (weak, nonatomic) IBOutlet UILabel *labelHeaderLanguage;
@property (weak, nonatomic) IBOutlet UILabel *labelHeaderSujet;
@property (weak, nonatomic) IBOutlet UILabel *labelHeaderQuantity;
@property (weak, nonatomic) IBOutlet UILabel *labelHeaderPrice;

@property (weak, nonatomic) IBOutlet UITableView *calculationTableView;
@property (weak, nonatomic) IBOutlet UIButton *addFormatButton;
@property (weak, nonatomic) IBOutlet UIButton *sendCalculationButton;
@property (weak, nonatomic) IBOutlet UILabel *labelDelivery;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelExclTax;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonDelete;

- (void) eventCalculationChanged: (NSNotification *) notification;
- (IBAction)eventAddProduct:(id)sender;
- (IBAction)eventSendCalculation:(id)sender;
- (IBAction)eventDeleteCalculation:(id)sender;

- (void) updateViews;

@end
