//
//  ProductListViewController.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 19.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductViewController.h"

#import "ProductManager.h"
#import "ProductListItem.h"

// TODO refactor normal/highlight/selected colors
// TODO fix selectProduct method

@implementation ProductListViewController

@synthesize products;
@synthesize isDetailList;

- (void)viewDidLoad
{
    // setup products
    products = [[ProductManager sharedInstance] getProducts];
    
//#if defined(TARGET_CC)
//    detailListSpacerIndex = 5;
//#endif
    
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    // deselect all selected items
    if (!isDetailList || selectProduct != nil) {
        for (NSIndexPath *indexPath in [self.collectionView indexPathsForSelectedItems]) {
            [self.collectionView deselectItemAtIndexPath: indexPath animated: NO];
            [self collectionView: self.collectionView didDeselectItemAtIndexPath: indexPath];
        }
    }
    
    // select item
    if (selectProduct != nil) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow: (NSInteger) [products indexOfObject: selectProduct] inSection: 0]; // FIXME hardcoded section
        [self.collectionView selectItemAtIndexPath:indexPath animated: YES scrollPosition: UICollectionViewScrollPositionCenteredVertically];
        [self collectionView: self.collectionView didSelectItemAtIndexPath: indexPath];
        selectProduct = nil;
    }
}

- (void) selectProduct: (Product *) product {
    selectProduct = product;
    // TODO check if view is visible
}

#pragma mark UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    // prevent selecting already selected item
    ProductListItem *cell = (ProductListItem *)[collectionView cellForItemAtIndexPath: indexPath];
    if (cell != nil && cell.selected) {
        return NO;
    }
    
    return YES;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = indexPath.row;
    
    // fix index because of spacer cell
    if (!isDetailList && detailListSpacerIndex > 0 && detailListSpacerIndex < index) {
        index -= 1;
    }
    
    Product *product = products[(NSUInteger)index];
    
    // if the parent view controller responds to the setProduct selector we will call it
    if ([self.parentViewController respondsToSelector: @selector(setProduct:)]) {
        [self.parentViewController performSelector:@selector(setProduct:) withObject: product];
    } else {
        // else we post a notification
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject: product forKey: @"product"];
        [[NSNotificationCenter defaultCenter] postNotificationName: EVENT_PRODUCT_DETAIL object: nil userInfo: userInfo];
    }
    
    ProductListItem *cell = (ProductListItem *)[collectionView cellForItemAtIndexPath: indexPath];
    cell.titleLabel.textColor = [UIColor whiteColor];
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // reset title color
    ProductListItem *cell = (ProductListItem *)[collectionView cellForItemAtIndexPath: indexPath];
    cell.titleLabel.textColor = BASE_COLOR;
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    ProductListItem *cell = (ProductListItem *)[collectionView cellForItemAtIndexPath: indexPath];
    cell.titleLabel.textColor = [UIColor whiteColor];
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    ProductListItem *cell = (ProductListItem *)[collectionView cellForItemAtIndexPath: indexPath];
    if (cell.selected) {
        cell.titleLabel.textColor = [UIColor whiteColor];
    } else {
        cell.titleLabel.textColor = BASE_COLOR;
    }
}

#pragma mark UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (!isDetailList && detailListSpacerIndex > 0) {
        return ((NSInteger) products.count) + 1;
    }
    return (NSInteger) products.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = indexPath.row;
    if (!isDetailList && detailListSpacerIndex > 0 && detailListSpacerIndex == index) {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"SpacerProductListCell" forIndexPath: indexPath];
        return cell;
    }
    
    // fix index because of spacer cell
    if (!isDetailList && detailListSpacerIndex > 0 && detailListSpacerIndex < index) {
        index -= 1;
    }
    
    
    ProductListItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"ProductListCell" forIndexPath: indexPath];
    
    cell.backgroundColor = PRODUCT_BACKGROUND_COLOR;
    cell.selectedBackgroundView = [[UIView alloc] init];
    cell.selectedBackgroundView.backgroundColor = BASE_COLOR;
    
    // TODO check of view update is necessary
    Product *product = products[(NSUInteger) index];
    
    cell.tag = index;
    cell.titleLabel.text = product.name;
    cell.infoLabel.text = product.info;
    
    // BECAUSE APPLE IS LAZY! THEY NEVER IMPLEMENTED CUSTOM FONTS IN THE INTERFACE BUILDER, FOR OVER 3 YEARS ...
    SET_BOLD_FONT(cell.titleLabel);
    SET_REGULAR_FONT(cell.infoLabel);
    
    if (cell.selected) {
        cell.titleLabel.textColor = [UIColor whiteColor];
    } else {
        cell.titleLabel.textColor = BASE_COLOR;
    }
    cell.imageView.image = [UIImage imageNamed: product.iconName];
    return cell;
}

@end
