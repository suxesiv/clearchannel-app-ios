//
//  CalculationItemConfig.
//  Kalkulationstool
//
//  Created by Rémy Böhler on 22.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculationItemConfig : NSObject

@property (nonatomic, strong) NSString *language;
@property (nonatomic) unsigned int sujets;
@property (nonatomic) unsigned int quantity;

- (NSString *) languageName;

@end
