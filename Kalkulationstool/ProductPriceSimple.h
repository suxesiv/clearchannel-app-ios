//
//  ProductPriceSimple.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 22.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductPriceInterface.h"

@interface ProductPriceSimple : ProductPriceInterface

@property (nonatomic) int productId;
@property (nonatomic) double piece;
@property (nonatomic) double sujet;

@end
