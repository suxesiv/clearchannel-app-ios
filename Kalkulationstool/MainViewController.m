//
//  MainViewController.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 19.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "MainViewController.h"

#import "CalculationViewController.h"

@implementation MainViewController

@synthesize introImage;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = CALCULATION_TITLE;
    introImage.image = [UIImage imageNamed: @"Intro"];
}

@end
