//
//  ProductManager.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 19.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FMDatabase.h"
@class Product;

@interface ProductManager : NSObject {
    NSMutableArray *products;
}

@property (nonatomic, strong) FMDatabase *database;

+ (ProductManager *)sharedInstance;

- (NSMutableArray *) getProducts;
- (Product *) getProductById: (int) productId;

@end
