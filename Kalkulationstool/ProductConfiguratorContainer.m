//
//  ProductConfiguratorContainer.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 21.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductConfiguratorContainer.h"

@implementation ProductConfiguratorContainer

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self customInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        [self customInit];
    }
    return self;
}

- (void) customInit {
    if (headContainer == nil) {
        return;
    }
    
    // head
    headContainer = [[UIView alloc] init];
    headContainer.backgroundColor = [UIColor purpleColor];
    [self addSubview: headContainer];
    
    // body
    bodyContainer = [[UIView alloc] init];
    bodyContainer.backgroundColor = [UIColor purpleColor];
    [self addSubview: bodyContainer];
}

@end
