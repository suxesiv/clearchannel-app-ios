//
//  CalculationTableViewCell.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 26.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelProduct;
@property (weak, nonatomic) IBOutlet UILabel *labelProductInfo;
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;

// config

// language
@property (weak, nonatomic) IBOutlet UILabel *labelLanguage0;
@property (weak, nonatomic) IBOutlet UILabel *labelLanguage1;
@property (weak, nonatomic) IBOutlet UILabel *labelLanguage2;

// sujet
@property (weak, nonatomic) IBOutlet UILabel *labelSujet0;
@property (weak, nonatomic) IBOutlet UILabel *labelSujet1;
@property (weak, nonatomic) IBOutlet UILabel *labelSujet2;

// quantity
@property (weak, nonatomic) IBOutlet UILabel *labelQuantity0;
@property (weak, nonatomic) IBOutlet UILabel *labelQuantity1;
@property (weak, nonatomic) IBOutlet UILabel *labelQuantity2;

@end
