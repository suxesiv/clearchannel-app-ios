//
//  CalculationItemConfig.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 22.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "CalculationItemConfig.h"

@implementation CalculationItemConfig

@synthesize language;
@synthesize sujets;
@synthesize quantity;

- (NSString *) languageName {
    // FIXME bad method
    if ([language isEqualToString: @"de"]) {
        return @"Deutsch";
    } else if ([language isEqualToString: @"fr"]) {
        return @"Französisch";
    } else if ([language isEqualToString: @"it"]) {
        return @"Italienisch";
    }
    NSLog(@"unknown language: %@", language);
    return @"[unknown]";
}

@end
