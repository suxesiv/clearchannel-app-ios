//
//  ProductPriceSimple.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 22.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductPriceSimple.h"

#import "CalculationItem.h"
#import "ProductManager.h"

#import "ProductPriceRule.h"

static NSString *const PRICE_TYPE = @"simple";

@implementation ProductPriceSimple

@synthesize productId;
@synthesize piece;
@synthesize sujet;

- (double) calculatePrice: (CalculationItem *) item {
    if ([item getTotalQuantity] <= 0 || [item getTotalSujets] <= 0) {
        return 0.0;
    }
    
    double price = 0.0;
    
    // Quantity
    price += [item getTotalQuantity] * piece;
    
    // Sujets
    price += [item getTotalSujets] * sujet;
    
    // special rules
    double itemPrice = price;
    for (ProductPriceRule *rule in [ProductPriceRule getByProductId: productId andPriceType: PRICE_TYPE]) {
        if ([rule appliesToItem: item]) {
            price += [rule calculateAmount: itemPrice];
        }
    }
    
    return price;
}

+ (ProductPriceSimple *) priceTypeForProduct: (int) productId {
    ProductManager *manager = [ProductManager sharedInstance];
    
    // query products form database
    FMResultSet *rs = [manager.database executeQuery:@"SELECT product_id, piece, sujet FROM product_price_simple WHERE product_id = ?", [NSNumber numberWithInt: productId]];
    if (rs == nil || ![rs next]) {
        @throw @"price type not found";
        return nil;
    }
    
    ProductPriceSimple *priceType = [[self alloc] init];
    
    priceType.productId = [rs intForColumn: @"product_id"];
    priceType.piece = [rs doubleForColumn: @"piece"];
    priceType.sujet = [rs doubleForColumn: @"sujet"];
    
    [rs close];
    
    return priceType;
}

@end
