//
//  ProductPriceInterface.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 22.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductPriceInterface.h"

@implementation ProductPriceInterface

+ (ProductPriceInterface *) priceTypeForProduct: (int) productId {
    [self doesNotRecognizeSelector: _cmd];
    return nil;
}

- (double) calculatePrice: (CalculationItem *) item {
    [self doesNotRecognizeSelector: _cmd];
    return 0.0f;
}

@end
