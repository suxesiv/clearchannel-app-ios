//
//  CalculationViewController.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 20.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "CalculationViewController.h"

#import "CalculationTableViewCell.h"
#import "Product.h"
#import "CalculationItemConfig.h"
#import "CurrencyFormatter.h"

@implementation CalculationViewController

@synthesize labelHeaderFormat, labelHeaderLanguage, labelHeaderSujet, labelHeaderQuantity, labelHeaderPrice;

@synthesize calculationTableView;
@synthesize addFormatButton;
@synthesize sendCalculationButton;
@synthesize labelDelivery, labelTotalPrice, labelExclTax;

@synthesize barButtonDelete;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // retrieve calculation
    calculation = [Calculation sharedInstance];
    
    // register event
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(eventCalculationChanged:) name:EVENT_CHANGE_CALCULATION object: nil];
    
    // replace fonts
    SET_REGULAR_FONT(labelHeaderFormat);
    SET_REGULAR_FONT(labelHeaderLanguage);
    SET_REGULAR_FONT(labelHeaderSujet);
    SET_REGULAR_FONT(labelHeaderQuantity);
    SET_REGULAR_FONT(labelHeaderPrice);
    
    // FIXME remove as soon Apple allows it to set custom fonts in IB
    SET_BOLD_FONT(addFormatButton.titleLabel);
    SET_BOLD_FONT(sendCalculationButton.titleLabel);
    
    addFormatButton.backgroundColor = BASE_COLOR;
    sendCalculationButton.backgroundColor = BASE_COLOR;
    
    SET_REGULAR_FONT(labelDelivery);
    SET_BOLDER_FONT(labelTotalPrice);
    SET_REGULAR_FONT(labelExclTax);
    
    labelHeaderFormat.textColor =
    labelHeaderLanguage.textColor =
    labelHeaderSujet.textColor =
    labelHeaderQuantity.textColor =
    labelHeaderPrice.textColor =
    labelDelivery.textColor =
    labelTotalPrice.textColor =
    labelExclTax.textColor = TEXT_COLOR;
    
    // check if we can compose a mail
    sendCalculationButton.hidden = ![MFMailComposeViewController canSendMail];
    
    // delivery cost
    deliveryCost = DELIVERY_COST;
    if (deliveryCost > 0) {
        CurrencyFormatter *currencyFormat = [[CurrencyFormatter alloc] init];
        labelDelivery.text = [NSString stringWithFormat: @"%@: %@", DELIVERY_TEXT, [currencyFormat stringFromNumber: [NSNumber numberWithDouble: deliveryCost]]];
    } else {
        labelDelivery.text = @"";
    }
    
    // set data
    [self updateViews];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    barButtonDelete.enabled = [calculation numItems] > 0;
}

- (void) updateViews {
    // reload table data
    [calculationTableView reloadData];
    
    // calculate total
    double totalPrice = [calculation getTotalPrice];
    labelTotalPrice.hidden = totalPrice <= 0;
    if (totalPrice > 0) {
        totalPrice += deliveryCost;
        NSString *totalPriceString = [[[CurrencyFormatter alloc] init] stringFromNumber: [NSNumber numberWithDouble: totalPrice]];
        labelTotalPrice.text = [NSString stringWithFormat: @"Total %@", totalPriceString];
    }
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (NSInteger) [calculation getItems].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CalculationTableViewItem";
    
    CalculationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: cellIdentifier forIndexPath: indexPath];
    if (cell == nil) {
        NSLog(@"haz no cell");
        // TODO create one?
    }
    
    // get item
    CalculationItem *item = [calculation getItemAtIndex: (NSUInteger) indexPath.row];
    Product *product = [item getProduct];
    
    // set data
    cell.labelProduct.text = product.name;
    cell.labelProductInfo.text = product.info;
    cell.labelPrice.text = [[[CurrencyFormatter alloc] init] stringFromNumber: [NSNumber numberWithDouble: [item getPrice]]];
    
    // config
    NSUInteger index = 0;
    UILabel *labelSujet, *labelQuantity;
    for (NSString *key in item.configs) {
        CalculationItemConfig *config = [item.configs objectForKey: key];
        
        /**
         * TODO make this better
         * probably array with the outlets?
         */
        switch (index) {
            case 0:
                labelSujet = cell.labelSujet0;
                labelQuantity = cell.labelQuantity0;
                break;
            case 1:
                labelSujet = cell.labelSujet1;
                labelQuantity = cell.labelQuantity1;
                break;
            case 2:
                labelSujet = cell.labelSujet2;
                labelQuantity = cell.labelQuantity2;
                break;
        }
        
        labelSujet.text = (config.sujets > 0 ? [NSString stringWithFormat: @"%d", config.sujets] : @"–");
        labelQuantity.text = (config.quantity > 0 ? [NSString stringWithFormat: @"%d", config.quantity] : @"–");
        
        index++;
    }
    
    return cell;
}

// selection
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    CalculationItem *item = [calculation getItemAtIndex: (NSUInteger) indexPath.row];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject: item forKey: @"item"];
    [[NSNotificationCenter defaultCenter] postNotificationName: EVENT_PRODUCT_DETAIL object: nil userInfo:userInfo];
}

// editing
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // delete item
        [calculation removeItemAtIndex: (NSUInteger) indexPath.row];
        [[NSNotificationCenter defaultCenter] postNotificationName: EVENT_CHANGE_CALCULATION object: nil];
        
        //[tableView deleteRowsAtIndexPaths: @[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Löschen";
}

- (void) eventCalculationChanged: (NSNotification *) notification {
    [self updateViews];
    
    // and show main if empty
    if ([calculation numItems] <= 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName: EVENT_SHOW_MAIN object:nil];
    }
}

- (IBAction)eventAddProduct:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName: EVENT_SHOW_MAIN object:nil];
}

#pragma mark send calculation
- (IBAction)eventSendCalculation:(id)sender {
    // Apple does nil the mail compose on first use, we need to reload it again!
    MFMailComposeViewController *messageViewController = [[MFMailComposeViewController alloc] init];
    messageViewController.mailComposeDelegate = self;
    
    [messageViewController setSubject: CALCULATION_MAIL_TITLE];
    
    NSMutableString *message = [NSMutableString stringWithString: CALCULATION_MAIL_TITLE];
    [message appendString: @"\n\n"];
    
    CurrencyFormatter *currencyFormat = [[CurrencyFormatter alloc] init];
    
    // items
    for (CalculationItem *item in [calculation getItems]) {
        Product *product = [item getProduct];
        [message appendFormat: @"Format: %@", product.name];
        if (product.info != nil && product.info.length > 0) {
            [message appendFormat: @" (%@)", product.info];
        }
        [message appendFormat: @"\n%@\n", product.description];
        
        // configs
        for (NSString *key in item.configs) {
            CalculationItemConfig *config = [item.configs objectForKey: key];
            
            NSString *languageName = [config languageName];
            [message appendFormat: @"%@ / %d Sujets / Auflage %d\n", languageName, config.sujets, config.quantity];
        }
        
        [message appendFormat: @"\nPreis: %@", [currencyFormat stringFromNumber: [NSNumber numberWithDouble: [item getPrice]]]];
        
        [message appendString: @"\n––––––––––––––––––\n"];
    }
    
    // delivery
    if (deliveryCost > 0) {
        [message appendFormat: @"%@: %@\n", DELIVERY_TEXT, [currencyFormat stringFromNumber: [NSNumber numberWithDouble: deliveryCost]]];
    }
    
    // add total
    [message appendFormat: @"Total exkl. MwST: %@\n\n", [currencyFormat stringFromNumber: [NSNumber numberWithDouble: [calculation getTotalPrice] + deliveryCost]]];
    
//#if defined(TARGET_CC)
//    [message appendString: @"Auftragsnummer: \n"];
//    [message appendString: @"Anlieferung bis: \n"];
//    [message appendString: @"Weitere Infos: \n"];
//    [message appendString: @"\n\n"];
//#endif
    
    // append info text
    [message appendString: CALCULATION_DISCLAIMER];
#if !defined(TARGET_CC)
        [message appendString: @"\n\n"];
        [message appendString: @"Ihr Kundenberater wird Ihnen gerne behilflich sein bei der Wahl der passenden Plakatdruckerei."];
#endif
    
    // set message body
    [messageViewController setMessageBody: message isHTML: NO];
    
    [self presentViewController: messageViewController animated: YES completion: nil];
}

- (IBAction)eventDeleteCalculation:(id)sender {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    // clear calculation
    [calculation empty];
    [center postNotificationName: EVENT_CHANGE_CALCULATION object: nil];
    
    // and show main
    //[center postNotificationName: EVENT_SHOW_MAIN object:nil];
}

#pragma mark MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    // TODO error handling
    //NSLog(@"Result: %d", result);
    if (error) {
        NSLog(@"ERROR: %@", error);
    }
    
    [self dismissViewControllerAnimated: YES completion: nil];
}

@end
