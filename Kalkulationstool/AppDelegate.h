//
//  AppDelegate.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 19.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MainViewController.h"
#import "RootNavigationController.h"
#import "ProductViewController.h"
#import "CalculationViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    RootNavigationController *rootViewController;
    MainViewController *mainViewController;
    ProductViewController *productViewController;
    CalculationViewController *calculationViewController;
}

@property (strong, nonatomic) UIWindow *window;

- (void) eventShowMain: (NSNotification *) notification;
- (void) eventShowProductDetail: (NSNotification *) notification;
- (void) eventShowCalculation: (NSNotification *) notification;

@end
