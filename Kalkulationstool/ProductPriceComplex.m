//
//  ProductPriceComplex.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 23.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductPriceComplex.h"

#import "CalculationItem.h"
#import "CalculationItemConfig.h"
#import "ProductManager.h"
#import "FMDatabase.h"

#import "ProductPriceRule.h"

static NSString *const PRICE_TYPE = @"complex";

@implementation ProductPriceComplex

@synthesize productId;
@synthesize base;
@synthesize piece;
@synthesize plateLanguage;
@synthesize plateSujet;

- (double) calculatePrice: (CalculationItem *) item {
    if ([item getTotalQuantity] <= 0 || [item getTotalSujets] <= 0) {
        return 0.0;
    }
    
    // base price
    double price = base;
    
    // pieces
    double pricePiece = [item getTotalQuantity] * piece;
    price += pricePiece;
    
    // sujets
    NSArray *sujets = [item getSujetCounts];
    
    double pricePlate = 0.0;
    int index = 0;
    for (NSNumber *numSujet in sujets) {
        int plates = [self getPlatesByIndex: index forSujets: numSujet];
        if (plates < 0) {
            NSLog(@"%s  - could not find plate config for [%d][%@] prodId: %d", __PRETTY_FUNCTION__, index, numSujet, productId);
        }
        if (index == 0) {
            /**
             * Die erste Sprache wird mit dem Preis plate_sujet gerechnet
             * Der Basispreis wird separat hinzugefügt, nicht wie im Excel bei der ersten Sprache
             */
            pricePlate += plates * plateSujet;
        } else {
            /**
             * Die weiteren Sprachen werden mit dem Preis plate_language verrechnet
             */
            pricePlate += plates * plateLanguage;
        }
        
        ++index;
    }
    price += pricePlate;
    
    /**
     * Spezialregeln werden auf den Basispreis plus den Preis der Platten berechnet.
     */
    double itemPrice = base + pricePlate;
    for (ProductPriceRule *rule in [ProductPriceRule getByProductId: productId andPriceType: PRICE_TYPE]) {
        if ([rule appliesToItem: item]) {
            price += [rule calculateAmount: itemPrice];
        }
    }
    
    return price;
}

- (int) getPlatesByIndex: (int) index forSujets: (NSNumber *) numSujet {
    ProductManager *manager = [ProductManager sharedInstance];
    
    FMResultSet *rs = [manager.database executeQuery:
                       @"SELECT plates FROM product_price_complex_plate WHERE product_id = ? AND language >= ? AND sujets = ? LIMIT 1",
                       [NSNumber numberWithInt: productId],
                       [NSNumber numberWithInt: index],
                       numSujet
                       ];
    
    if (rs == nil || ![rs next]) {
        @throw @"plate config not found";
        return -1;
    }
    
    int plates = [rs intForColumn: @"plates"];
    [rs close];
    
    return plates;
}

+ (ProductPriceComplex *) priceTypeForProduct: (int) productId {
    ProductManager *manager = [ProductManager sharedInstance];
    
    // query products form database
    FMResultSet *rs = [manager.database executeQuery:@"SELECT product_id, base, piece, plate_language, plate_sujet FROM product_price_complex WHERE product_id = ?", [NSNumber numberWithInt: productId]];
    if (rs == nil || ![rs next]) {
        @throw @"price type not found";
        return nil;
    }
    
    ProductPriceComplex *priceType = [[self alloc] init];
    
    priceType.productId = [rs intForColumn: @"product_id"];
    priceType.base = [rs doubleForColumn: @"base"];
    priceType.piece = [rs doubleForColumn: @"piece"];
    priceType.plateLanguage = [rs doubleForColumn: @"plate_language"];
    priceType.plateSujet = [rs doubleForColumn: @"plate_sujet"];
    
    [rs close];
    
    return priceType;
}

@end
