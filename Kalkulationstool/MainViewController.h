//
//  MainViewController.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 19.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "BaseViewController.h"

@class CalculationItem;

@interface MainViewController : BaseViewController {
    CalculationItem *calculationItem;
}

@property (weak, nonatomic) IBOutlet UIImageView *introImage;

@end
