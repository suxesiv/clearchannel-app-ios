//
//  ProductPriceRule.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 27.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductPriceRule.h"

#import "ProductManager.h"

// types
static NSString *const TYPE_BASIC = @"basic";
static NSString *const TYPE_ODD_SUJET = @"odd_sujet";

// price types
static NSString *const PRICE_TYPE_PERCENT = @"percent";
static NSString *const PRICE_TYPE_AMOUNT = @"amount";


@implementation ProductPriceRule

@synthesize ruleId;
@synthesize name;
@synthesize type;
@synthesize languages;
@synthesize sujets;
@synthesize priceType;
@synthesize percent;
@synthesize amount;

/**
 * Caution: This method does not check if the product in the item has the rule applied!
 *
 * @param item
 *        calculation item to check rule against
 * @return bool
 */
- (BOOL) appliesToItem: (CalculationItem *) item
{
    if ([type isEqualToString: TYPE_ODD_SUJET]) {
        /**
         * Die ungerade Anzahl Sujets Regel
         */
        
        NSArray *sujetCounts = [item getSujetCounts];
        
        NSNumber *firstLanguageSujets = sujetCounts[0];
        
        if (
            firstLanguageSujets.intValue % 2 == 1 || // wenn erste sprache ungerade ist zählt die regel nicht.
            sujetCounts.count < 2 // oder nur eine sprache ausgewählt wurde
            ) {
            return NO;
        }
        
        NSNumber *secondLanguageSujets = sujetCounts[1];
        
        switch (sujetCounts.count) {
            case 2:
                // bei 2 Sprachen, falls die zweite Sprache eine ungerade Anzahl Sujets hat zählt die Regel dazu
                return secondLanguageSujets.intValue % 2 == 1;
            case 3:
            {
                NSNumber *thirdLanguageSujets = sujetCounts[2];
                return
                secondLanguageSujets.intValue % 2 == 1 && // wenn 2. Sprache ungerade
                thirdLanguageSujets.intValue % 2 == 0 // und die 3. gerade
                ;
            }
        }
        
        return NO;
    } else { // $this->type == self::TYPE_BASIC
        /**
         * Basisregel
         * Check ob Anzahl Sprachen und (Max) Anzahl Sujets dem Item entspricht
         */
        return
        [item getTotalLanguages] == languages &&
        [item getMaxSujets] == sujets;
    }
}

/**
 * @param price
 *        The price to calculate the rule price
 * @return double
 */
- (double) calculateAmount: (double) price
{
    if ([priceType isEqualToString: PRICE_TYPE_AMOUNT]) {
        return amount;
    } else if ([priceType isEqualToString: PRICE_TYPE_PERCENT]) {
        NSAssert(price > 0.0, @"percent price needs an price to be calculated");
        return price * (percent / 100);
    } else {
        NSLog(@"%s – unknown type (%@)", __PRETTY_FUNCTION__, priceType);
    }
    return 0.0;
}

/**
 * @param productId
 *        Id of the product
 * @param priceType
 *        Price type to search rules for
 * @return Shop_Model_Product_Price_Rule[]
 */
+ (NSMutableArray *) getByProductId: (int) productId andPriceType: (NSString *) priceType
{
    ProductManager *manager = [ProductManager sharedInstance];
    
    // query rules form database
    FMResultSet *rs = [manager.database executeQuery:
                       @"SELECT\
                       rowid AS rule_id,\
                       name,\
                       type,\
                       languages,\
                       sujets,\
                       product_price_rule.price_type,\
                       percent,\
                       amount\
                       FROM product_product_price_rule\
                       LEFT JOIN product_price_rule ON rowid = price_rule_id\
                       WHERE product_id = ? AND product_product_price_rule.price_type = ?",
                       [NSNumber numberWithInt: productId],
                       priceType
                       ];
    
    if (rs == nil) {
        NSLog(@"ERROR, could not query rules");
        return nil;
    }
    
    NSMutableArray *result = [NSMutableArray arrayWithCapacity: 1]; // TODO correct capacity
    while ([rs next]) {
        ProductPriceRule *priceRule = [[self alloc] init];
        
        priceRule.ruleId = [rs intForColumn: @"rule_id"];
        priceRule.name = [rs stringForColumn: @"name"];
        priceRule.type = [rs stringForColumn: @"type"];
        priceRule.languages = (unsigned int)[rs unsignedLongLongIntForColumn: @"languages"];
        priceRule.sujets = (unsigned int)[rs unsignedLongLongIntForColumn: @"sujets"];
        priceRule.priceType = [rs stringForColumn: @"price_type"];
        priceRule.percent = [rs doubleForColumn: @"percent"];
        priceRule.amount = [rs doubleForColumn: @"amount"];
        
        [result addObject: priceRule];
    }
    [rs close];
    
    return result;
}



@end
