//
//  ProductPriceTierContainer.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 23.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductPriceTier.h"

#import "CalculationItem.h"
#import "CalculationItemConfig.h"
#import "ProductManager.h"
#import "FMDatabase.h"

#import "ProductPriceRule.h"

static NSString *const PRICE_TYPE = @"tier";

@implementation ProductPriceTier

@synthesize productId;
@synthesize base;
@synthesize priceSujet;

- (double) calculatePrice: (CalculationItem *) item {
    if ([item getTotalQuantity] <= 0 || [item getTotalSujets] <= 0) {
        return 0.0f;
    }
    
    // base price
    double price = self.base;
    
    // price per piece
    for (NSString *language in item.configs) {
        CalculationItemConfig *config = [item.configs objectForKey: language];
        if (config.quantity > 0) {
            double pricePiece = [self getPriceForQuantity: config.quantity];
            price += pricePiece * config.quantity;
        }
    }
    
    // config
    if (priceSujet > 0) {
        price += priceSujet * [item getTotalSujets];
    }
    
    // TODO special rules
    double itemPrice = price;
    for (ProductPriceRule *rule in [ProductPriceRule getByProductId: productId andPriceType: PRICE_TYPE]) {
        if ([rule appliesToItem: item]) {
            price += [rule calculateAmount: itemPrice];
        }
    }
    
    return price;
}

/**
 * The implementation of this method does not correspond with the php counterpart!
 *
 * In the php version the class holds an array of the data
 * here we query the database directly without storing anyting in the clas.
 */
- (double) getPriceForQuantity: (unsigned int) quantity {
    ProductManager *manager = [ProductManager sharedInstance];
    FMResultSet *rs = [manager.database executeQuery:@"SELECT price FROM product_price_tier WHERE product_id = ? AND pieces <= ? ORDER BY pieces DESC LIMIT 1", [NSNumber numberWithInt: productId], [NSNumber numberWithUnsignedInt: quantity]];

    if (rs == nil || ![rs next]) {
        NSLog(@"ERROR: did not found price for pid: %d / %d", productId, quantity);
        //@throw @"price type not found";
        return 0.0f;
    }
    
    double price = [rs doubleForColumn: @"price"];
    [rs close];
    
    return price;
}

+ (ProductPriceTier *) priceTypeForProduct: (int) productId {
    ProductManager *manager = [ProductManager sharedInstance];
    
    // query products form database
    FMResultSet *rs = [manager.database executeQuery:@"SELECT product_id, base, price_sujet FROM product_price_tier_config WHERE product_id = ?", [NSNumber numberWithInt: productId]];
    if (rs == nil || ![rs next]) {
        @throw @"price type not found";
        return nil;
    }
    
    ProductPriceTier *priceType = [[self alloc] init];
    
    priceType.productId = [rs intForColumn: @"product_id"];
    priceType.base = [rs doubleForColumn: @"base"];
    priceType.priceSujet = [rs doubleForColumn: @"price_sujet"];
    
    [rs close];
    
    return priceType;
}

@end
