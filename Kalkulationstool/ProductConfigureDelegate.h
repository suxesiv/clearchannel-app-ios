//
//  ProductConfigureDelegate.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 05.06.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ProductConfigureColumn;

@protocol ProductConfigureDelegate <NSObject>

- (void)configDidChange:(ProductConfigureColumn *)configView;
- (void) showNumberPickerFor: (UIView *)sender inConfig: (ProductConfigureColumn *)configView withValue: (uint) value;

@end
