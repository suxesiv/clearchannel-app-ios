//
//  ProductViewController.m
//  Kalkulationstool
//
//  Created by Rémy Böhler on 20.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import "ProductViewController.h"

#import "ProductManager.h"
#import "CalculationItemConfig.h"
#import "CalculationViewController.h"



@implementation ProductViewController

@synthesize titleLabel;
@synthesize descriptionLabel;
@synthesize priceLabel;
@synthesize sendButton;

@synthesize configView0, configView1, configView2;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // replace fonts
    // FIXME remove as soon Apple allows it to set custom fonts in IB
    SET_BOLDER_FONT(titleLabel);
    SET_BOLD_FONT(descriptionLabel);
    SET_BOLDER_FONT(priceLabel);
    SET_BOLD_FONT(sendButton.titleLabel);
    
    titleLabel.textColor =
    descriptionLabel.textColor =
    priceLabel.textColor = TEXT_COLOR;
    sendButton.backgroundColor = BASE_COLOR;
    
    // price format
    priceFormatter = [[CurrencyFormatter alloc] init];
    
    // number select
    numberPickerViewController = [[BigNumberPickerViewController alloc] init];
    numberPickerPopover = [[UIPopoverController alloc] initWithContentViewController: numberPickerViewController];
    numberPickerPopover.popoverContentSize = CGSizeMake(300, 216);
    
    // dirty initalisation of the config views
    configView0.delegate = self;
    configView0.language = @"de";
    configView0.titleLabel.text = @"Deutsch";
    
    configView1.delegate = self;
    configView1.language = @"fr";
    configView1.titleLabel.text = @"Französisch";
    
    configView2.delegate = self;
    configView2.language = @"it";
    configView2.titleLabel.text = @"Italienisch";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    // load the product
    [self updateProductViews];
}

- (void) calculationError: (NSString *) message {
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @"Kalkulationsfehler"
                          message: message
                          delegate: nil
                          cancelButtonTitle: @"OK"
                          otherButtonTitles: nil, nil];
    [alert show];
}

- (IBAction)eventAddToCalculation:(UIButton *)sender {
    BOOL hasError = NO;
    
    // check calculation
    if ([calculationItem getTotalSujets] <= 0) {
        hasError = YES;
        [self calculationError: @"Es muss mindestens 1 Sujet verwendet werden."];
    } else if ([calculationItem getTotalQuantity] <= 0) {
        hasError = YES;
        [self calculationError: @"Die Auflage muss mindestens 1 betragen."];
    }
    
    if (!hasError) {
        // add item to calculation
        // TODO check if already in calculation
        [[Calculation sharedInstance] addItem: calculationItem];
        
        // fire change event
        [[NSNotificationCenter defaultCenter] postNotificationName: EVENT_CHANGE_CALCULATION object: nil];
        
        // show calculation
        [[NSNotificationCenter defaultCenter] postNotificationName: EVENT_SHOW_CALCULATION object: nil];
    }
}

- (void) setCalculationItemAndSelect: (CalculationItem *) item {
    calculationItem = item;
    
    if (product == nil || product.productId != calculationItem.productId) {
        product = [[ProductManager sharedInstance] getProductById: calculationItem.productId];
    }
    selectProduct = YES;
    
    [self updateProductViews];
}

- (void)setProduct:(Product *)newProduct {
    if (newProduct != product) {
        product = newProduct;
        
        if (calculationItem == nil || calculationItem.productId != product.productId) {
            calculationItem = [CalculationItem createWithProduct: product];
        }
        
        if (self.view != nil) {
            [self updateProductViews];
        }
    }
}

- (void) setAndSelectProduct: (Product *) newProduct {
    selectProduct = YES;
    [self setProduct: newProduct];
}

- (void) updateProductViews {
    // product list
    if (listViewController != nil && selectProduct) {
        selectProduct = NO;
        [listViewController selectProduct: product];
    }
    
    // content
    titleLabel.text = product.name;
    descriptionLabel.text = product.description;
    
    // reset form
    CalculationItemConfig *config = [calculationItem getConfig: configView0.language];
    [configView0 setSujets: config.sujets andQuantity: config.quantity];
    
    config = [calculationItem getConfig: configView1.language];
    [configView1 setSujets: config.sujets andQuantity: config.quantity];
    
    config = [calculationItem getConfig: configView2.language];
    [configView2 setSujets: config.sujets andQuantity: config.quantity];
    
    // price
    [self updatePrice];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString: @"productListEmbed"]) {
        listViewController = segue.destinationViewController;
    }
}

#pragma mark product config delegate

- (void)configDidChange:(ProductConfigureColumn *)configView {
    //NSLog(@"did changes [%d, %d] for %@", configView.quantity, configView.sujets, configView.language);
    
    // update calculation item
    CalculationItemConfig *config = [calculationItem getConfig: configView.language];
    config.sujets = configView.sujets;
    config.quantity = configView.quantity;
    
    [self updatePrice];
}

- (void) updatePrice {
    // update price views
    double price = [calculationItem getPrice];
    priceLabel.hidden = price <= 0; // hide price label if no price available
    if (price > 0) {
        priceLabel.text = [priceFormatter stringFromNumber: [NSNumber numberWithDouble: price]];
    } else {
        //NSLog(@"empty price");
    }
}

- (void) showNumberPickerFor: (UIView *)sender inConfig: (ProductConfigureColumn *)configView withValue: (uint) value {
    numberPickerViewController.delegate = configView;
    [numberPickerViewController setPickerValue: value];
    [numberPickerPopover presentPopoverFromRect: sender.frame inView: configView permittedArrowDirections: UIPopoverArrowDirectionRight animated: YES];
}

@end
