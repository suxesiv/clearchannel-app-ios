//
//  ProductConfigureColumn.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 21.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ProductConfigureDelegate.h"

#pragma mark interface
@interface ProductConfigureColumn : UIView

@property (weak, nonatomic) id<ProductConfigureDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *quantityInput;
@property (weak, nonatomic) IBOutlet UIStepper *sujetStepper;
@property (weak, nonatomic) IBOutlet UILabel *sujetLabel;
@property (weak, nonatomic) IBOutlet UIButton *buttonQuantity;

// values
@property (nonatomic, strong) NSString *language;
@property (nonatomic) unsigned int quantity;
@property (nonatomic) unsigned int sujets;

- (void) reset;
- (void) setSujets: (unsigned int) newSujets andQuantity: (unsigned int) newQuantity;

- (IBAction)eventSujetChange:(UIStepper *)sender;
- (IBAction)eventClickQuantityButton:(UIButton *)sender;

- (void) updateQuantity: (unsigned int) newQuantity;

@end
