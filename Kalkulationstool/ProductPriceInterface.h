//
//  ProductPriceInterface.h
//  Kalkulationstool
//
//  Created by Römy Böhler on 22.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CalculationItem;

/**
 * This class should be an interface, but as objc does not allow me to
 * use a protocol as type, I decieded to use interface ...
 * Probably not the best solution but works for now.
 */
@interface ProductPriceInterface: NSObject

+ (ProductPriceInterface *) priceTypeForProduct: (int) productId;
- (double) calculatePrice: (CalculationItem *) item;

@end
