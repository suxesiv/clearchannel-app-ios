//
//  CurrencyFormatter.h
//  Kalkulationstool
//
//  Created by Rémy Böhler on 23.05.14.
//  Copyright (c) 2014 Suxesiv GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrencyFormatter : NSNumberFormatter

@end
